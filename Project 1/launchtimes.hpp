/*
	LaunchTimes.hpp - Header file for launchtimes.cpp that consists of the main functions used throughout the program
*/
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <iterator>
#include <Windows.h>
#include <process.h>
#include <iomanip>

using namespace std;

class LaunchTimes {

public:
	//create a map that will separate launch groups
	map<wstring, vector<pair<wstring, wstring>>> launchGroups;

	vector<HANDLE> handles;
	vector<PROCESS_INFORMATION> PIS;
	vector<wstring> errors;

	/*
		NAME:		ReadInputFile()
		PURPOSE:	Handles command line arguments passed in for test cases
					(we want to make sure that whatever is passed in is as file, and we want to terminate processes with a comma character)
		ACCEPTS:	a fileName of string type (file we are passing in)
		RETURNS:	bool as to whether or not it was a file and whether it was successfully launched
	*/
	bool ReadInputFile(string fileName) {
		wstring fileLine = L"";
		wstring launchGroup = L"";
		wstring programName = L"";
		wstring cmdParameters = L"";

		//check the parameter to make sure it's a file
		wifstream file(fileName);
		if (!file) {
			cout << "Could not find file " << fileName;
			return false;
		}//end if

		while (getline(file, fileLine))
		{
			launchGroup = L"";
			programName = L"";
			cmdParameters = L"";

			wstringstream stest(fileLine);
			getline(stest, launchGroup, L',');
			getline(stest, programName, L',');
			getline(stest, cmdParameters, L',');

			launchGroups[launchGroup].push_back(pair<wstring, wstring>(programName, cmdParameters));
		}//end while
		return true;
	}//ReadInputFile()

	 /*
		 NAME:		ProcessCreation()
		 PURPOSE:	Creates process(es) and sorts them based on whether the launch was a success, or a failure
		 ACCEPTS:	a vector of command line parameters of wstring type, as well as a launch group of type wstring
		 RETURNS:	N/A
	 */
	void ProcessCreation(vector<pair<wstring, wstring>> params, wstring launch) {
		STARTUPINFO sinfo = { 0 };
		sinfo.cb = sizeof(STARTUPINFO);
		unsigned long const CP_MAX_COMMANDLINE = 32768;
		for (unsigned i = 0; i < params.size(); i++)
		{
			PROCESS_INFORMATION PISOff;
			wstring pName = L"\"" + params[i].first + L"\" " + params[i].second; //can add space in program name
			wchar_t* commandLine = new wchar_t[CP_MAX_COMMANDLINE];
			wcsncpy_s(commandLine, CP_MAX_COMMANDLINE, pName.c_str(), pName.size() + 1);
			DWORD ReturnValue = CreateProcess(NULL,
				commandLine,
				NULL,
				NULL,
				false,
				CREATE_NEW_CONSOLE,
				NULL,
				NULL,
				&sinfo,
				&PISOff);

			delete[]commandLine;

			//handle the processes with errors
			if (ReturnValue == 0)
			{
				HRESULT hresult = GetLastError();
				LPWSTR  errorText = NULL;

				size_t size = FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER |
					FORMAT_MESSAGE_FROM_SYSTEM |
					FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL,
					hresult,
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					(LPWSTR)&errorText,
					0,
					NULL);

				if (errorText != NULL)
				{
					wstring message(errorText, size);
					wstring toPush = L"\n" + message + L"G: " + launch + L"\t" + pName;
					errors.push_back(toPush);
					LocalFree(errorText);
					errorText = NULL;
					CloseHandle(PISOff.hThread);
					CloseHandle(PISOff.hProcess);
					vector<pair<wstring,wstring>>::iterator iter = launchGroups[launch].begin() + i;
					if (iter != launchGroups[launch].end())
					{
						launchGroups[launch].erase(iter);
					}//end if
					else
					{
						launchGroups[launch].erase(launchGroups[launch].end() - 1);
					}//end else
				}//end if
			}//end if
			else {
				PIS.push_back(PISOff);
				handles.push_back(PIS.back().hProcess);
			}//end else
		}//end for
	}//ProcessCreation()

	 /*
		 NAME:		Output()
		 PURPOSE:	Formatted output of <Launch Group> <Kernel Time> <User Time> <Exit Code> <Program Name> <Command Line Parameters>
		 ACCEPTS:	A vector of command line parameters of wstring type, as well as a launch group of type wstring
		 RETURNS:	N/A
	 */
	void Output(vector<pair<wstring, wstring>> params, wstring launch) {
		for (unsigned i = 0; i < PIS.size(); i++)
		{
			FILETIME creationTime, exitTime, kernelTime, userTime;
			GetProcessTimes(PIS[i].hProcess, &creationTime, &exitTime, &kernelTime, &userTime);
			SYSTEMTIME kTime;
			FileTimeToSystemTime(&kernelTime, &kTime);
			SYSTEMTIME uTime;
			FileTimeToSystemTime(&userTime, &uTime);
			DWORD errCode;
			GetExitCodeProcess(PIS[i].hProcess, &errCode);
			pair<wstring, wstring> pNameAndParams = params[i];

			cout << "K:" << setfill('0') <<setw(2) << ":" << kTime.wMinute << ":" << setw(2) << kTime.wSecond << "." << setw(3) << kTime.wMilliseconds;
			cout << " U:" << setfill('0') << setw(2) << ":" << uTime.wMinute << ":" <<setw(2) << uTime.wSecond << "." << setw(3) << uTime.wMilliseconds << " E:" << errCode;
			wcout << " G:" << launch << " " << pNameAndParams.first << " " << pNameAndParams.second << "\n" << right;
			CloseHandle(PIS[i].hThread);
			CloseHandle(PIS[i].hProcess);
		}//end for
	}//Output()
};//LaunchTimes