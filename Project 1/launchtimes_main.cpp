/*
	Project 1 - ProcessLauncher
	Lexi Shaughnessy - 0658874
	October 8, 2015
	This program runs an application that launches and times a set of processes
*/

#include "launchtimes.hpp"

LaunchTimes lt;

int main(int argc, char *argv[]) {

	if (argc <= 1)
	{
		wcout << L"did not provide file" << endl; 
		return -1;
	}//end if

	if (!lt.ReadInputFile(argv[1]))
	{
		return EXIT_FAILURE;
	}//end if

	map<wstring, vector<pair<wstring, wstring>>>::iterator it;
	cout << "Launch Times:" << endl;
	for (it = lt.launchGroups.begin(); it != lt.launchGroups.end(); ++it) {
		try {
			lt.ProcessCreation(it->second, it->first);
			WaitForMultipleObjects(lt.handles.size(), lt.handles.data(), true, INFINITE);

			//I don't want to output the set that errored - that will display after the ones that have succeeded
			if (!lt.handles.empty()) {
				wcout << "\nG:" << setw(2) << left << it->first << endl;
			}//end if
			lt.Output(it->second, it->first);
		}//end try

		catch (exception ex) {
			cout << "Problem " << ex.what() << endl;
		}//end catch
		lt.handles.clear();
		lt.PIS.clear();
	}//end for

	if (!lt.errors.empty()) {
		cout << "\nErrors:" << endl;
	}//end if

	for (unsigned j = 0; j < lt.errors.size(); j++)
	{
		wcout << lt.errors[j] << endl;
	}//end for
}//main